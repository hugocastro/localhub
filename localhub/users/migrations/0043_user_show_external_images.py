# Generated by Django 3.0.4 on 2020-03-20 19:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("users", "0042_auto_20200310_0823"),
    ]

    operations = [
        migrations.AddField(
            model_name="user",
            name="show_external_images",
            field=models.BooleanField(default=True),
        ),
    ]
