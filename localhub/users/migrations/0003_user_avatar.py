# Generated by Django 2.2.2 on 2019-06-08 07:50

import sorl.thumbnail.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("users", "0002_auto_20190530_1027"),
    ]

    operations = [
        migrations.AddField(
            model_name="user",
            name="avatar",
            field=sorl.thumbnail.fields.ImageField(
                blank=True, null=True, upload_to="avatars"
            ),
        ),
    ]
