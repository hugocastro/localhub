# Generated by Django 2.2.2 on 2019-07-05 18:41

import django.utils.timezone
import model_utils.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("likes", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="like",
            name="created",
            field=model_utils.fields.AutoCreatedField(
                default=django.utils.timezone.now,
                editable=False,
                verbose_name="created",
            ),
        ),
        migrations.AddField(
            model_name="like",
            name="modified",
            field=model_utils.fields.AutoLastModifiedField(
                default=django.utils.timezone.now,
                editable=False,
                verbose_name="modified",
            ),
        ),
        migrations.AlterField(
            model_name="like", name="object_id", field=models.PositiveIntegerField(),
        ),
        migrations.AddIndex(
            model_name="like",
            index=models.Index(
                fields=["content_type", "object_id", "user"],
                name="likes_like_content_ddf715_idx",
            ),
        ),
        migrations.AddConstraint(
            model_name="like",
            constraint=models.UniqueConstraint(
                fields=("user", "content_type", "object_id"), name="unique_like"
            ),
        ),
    ]
