# Generated by Django 3.0.4 on 2020-03-16 13:01

from django.db import migrations

VERBS = (
    ("moderator_review_request", "moderator_review"),
    ("new_sibling_comment", "new_sibling"),
    ("replied_to_comment", "reply"),
    ("new_followed_user_comment", "followed_user"),
    ("new_followed_user_post", "followed_user"),
    ("new_followed_tag_post", "followed_tag"),
)

FOR_DELETION = ("moderator_edit",)


def rename_verbs(apps, schema_editor):
    Notification = apps.get_model("notifications.Notification")
    for verb in FOR_DELETION:
        Notification.objects.filter(verb=verb).delete()
    for old_verb, new_verb in VERBS:
        Notification.objects.filter(verb=old_verb).update(verb=new_verb)


class Migration(migrations.Migration):

    dependencies = [
        ("notifications", "0009_auto_20190820_0816"),
    ]

    operations = [migrations.RunPython(rename_verbs)]
